module Main (main) where

import Control.Monad (forever)
import Control.Concurrent (threadDelay)
import Network.Socket
import Network.Socket.ByteString (recvFrom)
import SignalDBus

import TargetGroup (group)

main :: IO ()
main = do
    ad <- addrAddress . head <$> getAddrInfo Nothing (Just "::") (Just "4000")

    withConn $ \sc -> forever $ do
        s  <- socket AF_INET6 Datagram defaultProtocol
        bind s ad
        (msg, peer) <- recvFrom s 64
        close s

        putStrLn $ "Nachricht bekommen von " ++ show peer ++ ": " ++ show msg
        sendGroupMessage "Ding dong!" [] group sc
        putStrLn "warte 30s..."
        threadDelay 30000000
        putStrLn "hab gewartet."
